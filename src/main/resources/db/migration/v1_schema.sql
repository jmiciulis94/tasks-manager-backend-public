-- DROP SCHEMA IF EXISTS tasks_manager CASCADE;

CREATE SCHEMA IF NOT EXISTS tasks_manager;

CREATE TABLE tasks_manager.user (
    id BIGSERIAL NOT NULL,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    username VARCHAR(50) NOT NULL,
    password VARCHAR(256) NOT NULL,
    email VARCHAR(256),
    currency VARCHAR(3) NOT NULL,
    date_format VARCHAR(20) NOT NULL,
    number_format VARCHAR(20) NOT NULL,
    period_type VARCHAR(10) NOT NULL DEFAULT 'MONTHLY' CHECK (period_type IN ('MONTHLY', 'WEEKLY')),
    first_day_of_the_week VARCHAR(10) NOT NULL DEFAULT 'MONDAY' CHECK (first_day_of_the_week IN ('MONDAY', 'SUNDAY')),
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.period (
    id BIGSERIAL NOT NULL,
    year INTEGER NOT NULL,
    month INTEGER,
    week INTEGER,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    user_id BIGINT REFERENCES tasks_manager.user(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.employee (
    id BIGSERIAL NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100),
    email VARCHAR(256),
    phone VARCHAR(15),
    date_of_birth DATE,
    start_date DATE DEFAULT CURRENT_DATE,
    type_of_pay VARCHAR(20) NOT NULL DEFAULT 'NONE' CHECK (type_of_pay IN ('RATE', 'HOURLY', 'NONE')),
    rate NUMERIC(8, 2),
    pay_per_hour NUMERIC(8, 2),
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    user_id BIGINT REFERENCES tasks_manager.user(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.speciality (
    id BIGSERIAL NOT NULL,
    title VARCHAR(50) NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    user_id BIGINT REFERENCES tasks_manager.user(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.employee_speciality (
    employee_id BIGINT NOT NULL REFERENCES tasks_manager.employee(id),
    speciality_id BIGINT NOT NULL REFERENCES tasks_manager.speciality(id),
    PRIMARY KEY (employee_id, speciality_id)
);

CREATE TABLE tasks_manager.project (
    id BIGSERIAL NOT NULL,
    title VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    estimated_cost NUMERIC(12, 2),
    start_date TIMESTAMP,
    estimated_end_date TIMESTAMP,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    user_id BIGINT REFERENCES tasks_manager.user(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.task (
    id BIGSERIAL NOT NULL,
    title VARCHAR(100) NOT NULL,
    is_paid_by_fact BOOLEAN NOT NULL DEFAULT FALSE,
    estimated_cost NUMERIC(12, 2),
    start_date TIMESTAMP,
    estimated_end_date TIMESTAMP,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    project_id BIGINT NOT NULL REFERENCES tasks_manager.project(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.period_budget (
    id BIGSERIAL NOT NULL,
    amount NUMERIC(15, 2),
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    period_id BIGINT NOT NULL REFERENCES tasks_manager.period(id),
    task_id BIGINT NOT NULL REFERENCES tasks_manager.task(id),
    PRIMARY KEY(id)
);

CREATE TABLE tasks_manager.timecard (
    id BIGSERIAL NOT NULL,
    description VARCHAR(500),
    minutes INTEGER NOT NULL,
    date TIMESTAMP NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    task_id BIGINT NOT NULL REFERENCES tasks_manager.task(id),
    employee_id BIGINT NOT NULL REFERENCES tasks_manager.employee(id),
    PRIMARY KEY (id)
);

CREATE TABLE tasks_manager.timesheet (
    id BIGSERIAL NOT NULL,
    minutes INTEGER NOT NULL,
    amount NUMERIC(15, 2),
    type_of_pay VARCHAR(20) NOT NULL DEFAULT 'NONE' CHECK (type_of_pay IN ('RATE', 'HOURLY', 'NONE')),
    rate NUMERIC(8, 2),
    pay_per_hour NUMERIC(8, 2),
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    period_id BIGINT NOT NULL REFERENCES tasks_manager.period(id),
    task_id BIGINT NOT NULL REFERENCES tasks_manager.task(id),
    employee_id BIGINT NOT NULL REFERENCES tasks_manager.employee(id),
    PRIMARY KEY (id)
);
