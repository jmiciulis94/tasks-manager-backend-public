package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Employee;
import net.smarttechsolutions.tasksmanager.backend.entity.EmployeeEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.SpecialityRepository;
import net.smarttechsolutions.tasksmanager.backend.utils.AgeUtils;
import net.smarttechsolutions.tasksmanager.backend.utils.PayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeConverter {

    @Autowired
    private SpecialityRepository specialityRepository;

    public EmployeeEntity convertToEntity(Employee employee) {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        LocalDate dateNow = LocalDate.now();
        List<SpecialityEntity> specialityList = new ArrayList<>(specialityRepository.findByIdIn(employee.getSpecialityIds()));

        employeeEntity.setId(employee.getId());
        employeeEntity.setFirstName(employee.getFirstName());
        employeeEntity.setLastName(employee.getLastName());
        employeeEntity.setDateOfBirth(employee.getDateOfBirth());
        employeeEntity.setEmail(employee.getEmail());
        employeeEntity.setPhone(employee.getPhone());
        if (employee.getStartDate() == null) {
            employeeEntity.setStartDate(dateNow);
        } else {
            employeeEntity.setStartDate(employee.getStartDate());
        }
        employeeEntity.setTypeOfPay(employee.getTypeOfPay());
        employeeEntity.setRate(employee.getRate());
        employeeEntity.setPayPerHour(employee.getPayPerHour());
        employeeEntity.setSpecialities(specialityList);

        return employeeEntity;
    }

    public Employee convertToDto(EmployeeEntity employeeEntity) {

        Employee employee = new Employee();
        AgeUtils ageUtils = new AgeUtils();
        PayUtils payUtils = new PayUtils();
        List<Long> specialityIds = new ArrayList<>();
        List<String> specialityTitles = new ArrayList<>();
        for (SpecialityEntity speciality : employeeEntity.getSpecialities()) {
            specialityTitles.add(speciality.getTitle());
        }
        List<SpecialityEntity> specialityList = employeeEntity.getSpecialities();
        for (SpecialityEntity speciality : specialityList) {
            specialityIds.add(speciality.getId());
        }

        employee.setId(employeeEntity.getId());
        employee.setFirstName(employeeEntity.getFirstName());
        employee.setLastName(employeeEntity.getLastName());
        employee.setDateOfBirth(employeeEntity.getDateOfBirth());
        employee.setEmail(employeeEntity.getEmail());
        employee.setPhone(employeeEntity.getPhone());
        if (employeeEntity.getDateOfBirth() != null) {
            employee.setAge(ageUtils.ageFromDateOfBirth(employeeEntity.getDateOfBirth()));
        }
        employee.setStartDate(employeeEntity.getStartDate());
        employee.setTypeOfPay(employeeEntity.getTypeOfPay());
        employee.setPayPerHour(employeeEntity.getPayPerHour());
        employee.setRate(employeeEntity.getRate());
        employee.setPayRate(payUtils.getPayRate(employeeEntity.getTypeOfPay(), employeeEntity.getRate(), employeeEntity.getPayPerHour()));
        employee.setSpecialityIds(specialityIds);
        employee.setSpecialityTitles(specialityTitles);

        return employee;
    }
}
