package net.smarttechsolutions.tasksmanager.backend.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import net.smarttechsolutions.tasksmanager.backend.dto.Project;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.ProjectEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectConverter {

    @Autowired
    private TaskConverter taskConverter;

    public ProjectEntity convertToEntity(Project project) {

        ProjectEntity projectEntity = new ProjectEntity();
        LocalDate now = LocalDate.now();
        List<TaskEntity> taskEntityList = new ArrayList<>();
        for (Task task : project.getTasks()) {
            TaskEntity taskEntity = taskConverter.convertToEntity(task);
            taskEntityList.add(taskEntity);
        }
        projectEntity.setId(project.getId());
        projectEntity.setTitle(project.getTitle());
        projectEntity.setDescription(project.getDescription());
        projectEntity.setEstimatedCost(project.getEstimatedCost());
        if (project.getStartDate() == null) {
            projectEntity.setStartDate(now);
        } else {
            projectEntity.setStartDate(project.getStartDate());
        }
        projectEntity.setEstimatedEndDate(project.getEstimatedEndDate());
        projectEntity.setTasks(taskEntityList);

        return projectEntity;
    }

    public Project convertToDto(ProjectEntity projectEntity) {

        Project project = new Project();
        List<Task> taskList = new ArrayList<>();
        for (TaskEntity taskEntity : projectEntity.getTasks()) {
            Task task = taskConverter.convertToDto(taskEntity);
            taskList.add(task);
        }
        project.setId(projectEntity.getId());
        project.setTitle(projectEntity.getTitle());
        project.setDescription(projectEntity.getDescription());
        project.setEstimatedCost(projectEntity.getEstimatedCost());
        project.setStartDate(projectEntity.getStartDate());
        project.setEstimatedEndDate(projectEntity.getEstimatedEndDate());
        project.setTasks(taskList);

        return project;
    }
}
