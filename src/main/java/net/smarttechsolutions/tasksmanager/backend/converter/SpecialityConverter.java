package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Speciality;
import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import org.springframework.stereotype.Component;

@Component
public class SpecialityConverter {

    public SpecialityEntity convertToEntity(Speciality speciality) {

        SpecialityEntity specialityEntity = new SpecialityEntity();
        specialityEntity.setId(speciality.getId());
        specialityEntity.setTitle(speciality.getTitle());

        return specialityEntity;
    }

    public Speciality convertToDto(SpecialityEntity specialityEntity) {

        Speciality speciality = new Speciality();
        speciality.setId(specialityEntity.getId());
        speciality.setTitle(specialityEntity.getTitle());

        return speciality;
    }
}
