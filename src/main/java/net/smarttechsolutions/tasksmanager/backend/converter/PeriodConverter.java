package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Period;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import org.springframework.stereotype.Component;

@Component
public class PeriodConverter {

    public PeriodEntity convertToEntity(Period period) {

        PeriodEntity periodEntity = new PeriodEntity();

        periodEntity.setId(period.getId());
        periodEntity.setYear(period.getYear());
        periodEntity.setMonth(period.getMonth());
        periodEntity.setWeek(period.getWeek());

        return periodEntity;
    }

    public Period convertToDto(PeriodEntity periodEntity) {

        Period period = new Period();

        period.setId(periodEntity.getId());
        period.setYear(periodEntity.getYear());
        period.setMonth(periodEntity.getMonth());
        period.setWeek(periodEntity.getWeek());

        return period;
    }
}
