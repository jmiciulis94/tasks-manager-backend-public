package net.smarttechsolutions.tasksmanager.backend.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import net.smarttechsolutions.tasksmanager.backend.dto.PeriodBudget;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodBudgetEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.PeriodRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.ProjectRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskConverter {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private PeriodRepository periodRepository;

    @Autowired
    private PeriodBudgetConverter periodBudgetConverter;

    public TaskEntity convertToEntity(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        LocalDate now = LocalDate.now();
        List<PeriodBudgetEntity> periodBudgetEntityList = new ArrayList<>();
        List<PeriodEntity> periodEntityList = periodRepository.findAllByDeletedFalse();
        for (PeriodBudget periodBudget : task.getPeriodBudgets()) {
            PeriodBudgetEntity periodBudgetEntity = periodBudgetConverter.convertToEntity(periodBudget, periodEntityList);
            periodBudgetEntityList.add(periodBudgetEntity);
        }

        taskEntity.setId(task.getId());
        taskEntity.setTitle(task.getTitle());
        taskEntity.setIsPaidByFact(task.getIsPaidByFact());
        taskEntity.setEstimatedCost(task.getEstimatedCost());
        if (task.getStartDate() == null) {
            taskEntity.setStartDate(now);
        } else {
            taskEntity.setStartDate(task.getStartDate());
        }
        taskEntity.setEstimatedEndDate(task.getEstimatedEndDate());
        taskEntity.setProject(projectRepository.findById(task.getProjectId()));
        taskEntity.setPeriodBudgets(periodBudgetEntityList);

        return taskEntity;
    }

    public Task convertToDto(TaskEntity taskEntity) {
        Task task = new Task();
        List<PeriodBudget> periodBudgetList = new ArrayList<>();
        for (PeriodBudgetEntity periodBudgetEntity : taskEntity.getPeriodBudgets()) {
            PeriodBudget periodBudget = periodBudgetConverter.convertToDto(periodBudgetEntity);
            periodBudgetList.add(periodBudget);
        }

        task.setId(taskEntity.getId());
        task.setTitle(taskEntity.getTitle());
        task.setIsPaidByFact(taskEntity.getIsPaidByFact());
        task.setEstimatedCost(taskEntity.getEstimatedCost());
        task.setStartDate(taskEntity.getStartDate());
        task.setEstimatedEndDate(taskEntity.getEstimatedEndDate());
        task.setProjectId(taskEntity.getProject().getId());
        task.setPeriodBudgets(periodBudgetList);

        return task;
    }
}
