package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Speciality;
import net.smarttechsolutions.tasksmanager.backend.dto.User;
import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserConverter {

    @Autowired
    private SpecialityConverter specialityConverter;

    public UserEntity convertToEntity(User user) {

        UserEntity userEntity = new UserEntity();
        List<SpecialityEntity> specialityEntityList = new ArrayList<>();

        for (Speciality speciality : user.getSpecialities()) {
            SpecialityEntity specialityEntity = specialityConverter.convertToEntity(speciality);
            specialityEntityList.add(specialityEntity);
        }

        userEntity.setId(user.getId());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setUsername(user.getUsername());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail(user.getEmail());
        userEntity.setCurrency(user.getCurrency());
        userEntity.setDateFormat(user.getDateFormat());
        userEntity.setNumberFormat(user.getNumberFormat());
        userEntity.setPeriodType(user.getPeriodType());
        userEntity.setFirstDayOfTheWeek(user.getFirstDayOfTheWeek());
        userEntity.setSpecialities(specialityEntityList);

        return userEntity;
    }

    public User convertToDto(UserEntity userEntity) {

        User user = new User();
        List<Speciality> specialityList = new ArrayList<>();

        for (SpecialityEntity specialityEntity : userEntity.getSpecialities()) {
            if (!specialityEntity.getDeleted()) {
                Speciality speciality = specialityConverter.convertToDto(specialityEntity);
                specialityList.add(speciality);
            }
        }

        user.setId(userEntity.getId());
        user.setFirstName(userEntity.getFirstName());
        user.setLastName(userEntity.getLastName());
        user.setUsername(userEntity.getUsername());
        user.setPassword(userEntity.getPassword());
        user.setEmail(userEntity.getEmail());
        user.setCurrency(userEntity.getCurrency());
        user.setDateFormat(userEntity.getDateFormat());
        user.setNumberFormat(userEntity.getNumberFormat());
        user.setPeriodType(userEntity.getPeriodType());
        user.setFirstDayOfTheWeek(userEntity.getFirstDayOfTheWeek());
        user.setSpecialities(specialityList);

        return user;
    }

}
