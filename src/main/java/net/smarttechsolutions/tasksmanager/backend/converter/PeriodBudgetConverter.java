package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.PeriodBudget;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodBudgetEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PeriodBudgetConverter {

    public PeriodBudgetEntity convertToEntity(PeriodBudget periodBudget, List<PeriodEntity> periodList) {

        PeriodBudgetEntity periodBudgetEntity = new PeriodBudgetEntity();
        PeriodEntity periodEntity = periodList.stream().filter(p -> p.getYear().equals(periodBudget.getYear()) && p.getMonth().equals(periodBudget.getMonth()) || p.getWeek().equals(periodBudget.getWeek())).findFirst().orElse(null);
        if (periodEntity == null) {
            periodEntity = new PeriodEntity();
            periodEntity.setYear(periodBudget.getYear());
            periodEntity.setMonth(periodBudget.getMonth());
            periodEntity.setWeek(periodBudget.getWeek());
        }
        periodBudgetEntity.setId(periodBudget.getId());
        periodBudgetEntity.setAmount(periodBudget.getAmount());
        periodBudgetEntity.setPeriod(periodEntity);

        return periodBudgetEntity;
    }

    public PeriodBudget convertToDto(PeriodBudgetEntity periodBudgetEntity) {

        PeriodBudget periodBudget = new PeriodBudget();

        periodBudget.setId(periodBudgetEntity.getId());
        periodBudget.setAmount(periodBudgetEntity.getAmount());
        periodBudget.setYear(periodBudgetEntity.getPeriod().getYear());
        periodBudget.setMonth(periodBudgetEntity.getPeriod().getMonth());
        periodBudget.setWeek(periodBudgetEntity.getPeriod().getWeek());

        return periodBudget;
    }
}
