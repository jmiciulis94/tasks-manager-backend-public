package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Timesheet;
import net.smarttechsolutions.tasksmanager.backend.entity.TimesheetEntity;
import net.smarttechsolutions.tasksmanager.backend.utils.PayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TimesheetConverter {

    @Autowired
    private PeriodConverter periodConverter;

    public Timesheet convertToDto(TimesheetEntity timesheetEntity) {

        Timesheet timesheet = new Timesheet();
        PayUtils payUtils = new PayUtils();

        timesheet.setId(timesheetEntity.getId());
        timesheet.setMinutes(timesheetEntity.getMinutes());
        timesheet.setAmount(timesheetEntity.getAmount());
        timesheet.setTypeOfPay(timesheetEntity.getTypeOfPay());
        timesheet.setRate(timesheetEntity.getRate());
        timesheet.setPayPerHour(timesheetEntity.getPayPerHour());
        timesheet.setPeriod(periodConverter.convertToDto(timesheetEntity.getPeriod()));
        timesheet.setPayRate(payUtils.getPayRate(timesheetEntity.getTypeOfPay(), timesheetEntity.getRate(), timesheetEntity.getPayPerHour()));
        timesheet.setTaskId(timesheetEntity.getTask().getId());
        timesheet.setEmployeeId(timesheetEntity.getEmployee().getId());

        return timesheet;
    }
}
