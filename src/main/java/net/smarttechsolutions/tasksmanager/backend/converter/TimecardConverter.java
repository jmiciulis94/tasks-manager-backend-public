package net.smarttechsolutions.tasksmanager.backend.converter;

import net.smarttechsolutions.tasksmanager.backend.dto.Timecard;
import net.smarttechsolutions.tasksmanager.backend.entity.TimecardEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.EmployeeRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TimecardConverter {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public TimecardEntity convertToEntity(Timecard timecard) {

        TimecardEntity timecardEntity = new TimecardEntity();

        timecardEntity.setId(timecard.getId());
        timecardEntity.setDescription(timecard.getDescription());
        timecardEntity.setMinutes(timecard.getMinutes());
        timecardEntity.setDate(timecard.getDate());
        timecardEntity.setTask(taskRepository.findById(timecard.getTaskId()));
        timecardEntity.setEmployee(employeeRepository.findById(timecard.getEmployeeId()));

        return timecardEntity;
    }

    public Timecard convertToDto(TimecardEntity timecardEntity) {

        Timecard timecard = new Timecard();

        timecard.setId(timecardEntity.getId());
        timecard.setDescription(timecardEntity.getDescription());
        timecard.setMinutes(timecardEntity.getMinutes());
        timecard.setDate(timecardEntity.getDate());
        timecard.setTaskId(timecardEntity.getTask().getId());
        timecard.setTaskTitle(timecardEntity.getTask().getTitle());
        timecard.setEmployeeId(timecardEntity.getEmployee().getId());
        timecard.setEmployeeFullName(timecardEntity.getEmployee().getFirstName() + " " + timecardEntity.getEmployee().getLastName());
        timecard.setProjectId(timecardEntity.getTask().getProject().getId());
        timecard.setProjectTitle(timecardEntity.getTask().getProject().getTitle());

        return timecard;
    }
}
