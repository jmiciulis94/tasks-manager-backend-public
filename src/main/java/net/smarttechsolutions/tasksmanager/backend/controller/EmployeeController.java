package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.EmployeeConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Employee;
import net.smarttechsolutions.tasksmanager.backend.dto.EmployeeReport;
import net.smarttechsolutions.tasksmanager.backend.entity.EmployeeEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.EmployeeService;
import net.smarttechsolutions.tasksmanager.backend.service.TimesheetService;
import net.smarttechsolutions.tasksmanager.backend.validator.EmployeeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class EmployeeController {

    @Autowired
    private EmployeeConverter employeeConverter;

    @Autowired
    private EmployeeValidator employeeValidator;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TimesheetService timesheetService;

    @GetMapping("employee/{id}")
    public Employee getEmployeeById(@PathVariable("id") Long id) {
        EmployeeEntity employeeEntity = employeeService.getById(id);
        return employeeConverter.convertToDto(employeeEntity);
    }

    @GetMapping("employee")
    public List<Employee> findAllEmployees() {
        return employeeService.getAll();
    }

    @PostMapping("employee")
    public Employee saveEmployee(@RequestBody Employee employee) {
        validate(employee, "employee");
        EmployeeEntity result = employeeService.save(employee);
        return employeeConverter.convertToDto(result);
    }

    @GetMapping("report/employee/{id}")
    public EmployeeReport getEmployeeReport(@PathVariable("id") Long employeeId) {
        return timesheetService.getEmployeeReport(employeeId);
    }

    @DeleteMapping("employee/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable("id") Long id) {
        EmployeeEntity employeeEntity = employeeService.getById(id);
        employeeService.delete(employeeEntity);
    }

    private void validate(Employee employee, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(employee, dtoName);
        employeeValidator.validate(employee, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
