package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.TimecardConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Timecard;
import net.smarttechsolutions.tasksmanager.backend.entity.TimecardEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.TimecardService;
import net.smarttechsolutions.tasksmanager.backend.validator.TimecardValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class TimecardController {

    @Autowired
    private TimecardValidator timecardValidator;

    @Autowired
    private TimecardConverter timecardConverter;

    @Autowired
    private TimecardService timecardService;

    @GetMapping("timecard/{id}")
    public Timecard getTimecardById(@PathVariable("id") Long id) {
        TimecardEntity timecardEntity = timecardService.getById(id);
        return timecardConverter.convertToDto(timecardEntity);
    }

    @GetMapping("timecard")
    public List<Timecard> findAllTimecards(@RequestParam("employeeId") Long employeeId) {
        return timecardService.getAllByEmployeeId(employeeId);
    }

    @PostMapping("timecard")
    public Timecard saveTimecard(@RequestBody Timecard timecard) {
        validate(timecard, "timecard");
        TimecardEntity result = timecardService.save(timecard);
        return timecardConverter.convertToDto(result);
    }

    @DeleteMapping("timecard/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTimecard(@PathVariable("id") Long id) {
        TimecardEntity timecardEntity = timecardService.getById(id);
        timecardService.delete(timecardEntity);
    }

    private void validate(Timecard timecard, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(timecard, dtoName);
        timecardValidator.validate(timecard, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
