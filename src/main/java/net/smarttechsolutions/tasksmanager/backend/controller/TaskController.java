package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.TaskConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.dto.TaskReport;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.TaskService;
import net.smarttechsolutions.tasksmanager.backend.service.TimesheetService;
import net.smarttechsolutions.tasksmanager.backend.validator.TaskValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class TaskController {

    @Autowired
    private TaskConverter taskConverter;

    @Autowired
    private TaskValidator taskValidator;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TimesheetService timesheetService;

    @GetMapping("task/{id}")
    public Task getTaskById(@PathVariable("id") Long id) {
        TaskEntity taskEntity = taskService.getById(id);
        return taskConverter.convertToDto(taskEntity);
    }

    @GetMapping("task")
    public List<Task> findAllTasks() {
        return taskService.getAll();
    }

    @PostMapping("task")
    public Task saveTask(@RequestBody Task task) {
        validate(task, "task");
        TaskEntity result = taskService.save(task);
        return taskConverter.convertToDto(result);
    }

    @GetMapping("report/task/{id}")
    public TaskReport getTaskReport(@PathVariable("id") Long id) {
        return timesheetService.getTaskReport(id);
    }

    @DeleteMapping("task/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable("id") Long id) {
        TaskEntity taskEntity = taskService.getById(id);
        taskService.delete(taskEntity);
    }

    private void validate(Task task, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(task, dtoName);
        taskValidator.validate(task, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
