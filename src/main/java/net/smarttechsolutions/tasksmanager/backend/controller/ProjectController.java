package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.ProjectConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Project;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.ProjectEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.ProjectService;
import net.smarttechsolutions.tasksmanager.backend.validator.ProjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class ProjectController {

    @Autowired
    private ProjectConverter projectConverter;

    @Autowired
    private ProjectValidator projectValidator;

    @Autowired
    private ProjectService projectService;

    @GetMapping("project/{id}")
    public Project getProjectById(@PathVariable("id") Long id) {
        ProjectEntity projectEntity = projectService.getById(id);
        return projectConverter.convertToDto(projectEntity);
    }

    @GetMapping("project/{id}/task")
    public List<Task> getTasksByProjectId(@PathVariable("id") Long id) {
        return projectService.getTasksByProjectId(id);
    }

    @GetMapping("project")
    public List<Project> findAllProjects() {
        return projectService.getAll();
    }

    @PostMapping("project")
    public Project saveProject(@RequestBody Project project) {
        validate(project, "project");
        ProjectEntity result = projectService.save(project);
        return projectConverter.convertToDto(result);
    }

    @DeleteMapping("project/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable("id") Long id) {
        ProjectEntity projectEntity = projectService.getById(id);
        projectService.delete(projectEntity);
    }

    private void validate(Project project, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(project, dtoName);
        projectValidator.validate(project, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
