package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.UserConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.User;
import net.smarttechsolutions.tasksmanager.backend.entity.UserEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.UserService;
import net.smarttechsolutions.tasksmanager.backend.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class UserController {

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private UserConverter userConverter;

    @GetMapping("user/{id}")
    public User getUserById(@PathVariable("id") Long id) {
        UserEntity userEntity = userService.getById(id);
        return userConverter.convertToDto(userEntity);
    }

    @GetMapping("user")
    public List<User> findAllUsers() {
        return userService.getAll();
    }

    @PostMapping("user")
    public User saveUser(@RequestBody User user) {
        validate(user, "user");
        UserEntity result = userService.save(user);
        return userConverter.convertToDto(result);
    }

    @DeleteMapping("user/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("id") Long id) {
        UserEntity userEntity = userService.getById(id);
        userService.delete(userEntity);
    }

    private void validate(User user, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(user, dtoName);
        userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
