package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.SpecialityConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Speciality;
import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import net.smarttechsolutions.tasksmanager.backend.service.SpecialityService;
import net.smarttechsolutions.tasksmanager.backend.validator.SpecialityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class SpecialityController {

    @Autowired
    private SpecialityValidator specialityValidator;

    @Autowired
    private SpecialityService specialityService;

    @Autowired
    private SpecialityConverter specialityConverter;

    @GetMapping("speciality/{id}")
    public Speciality getSpecialityById(@PathVariable("id") Long id) {
        SpecialityEntity specialityEntity = specialityService.getById(id);
        return specialityConverter.convertToDto(specialityEntity);
    }

    @GetMapping("speciality")
    public List<Speciality> findAllSpecialities() {
        return specialityService.getAll();
    }

    @PostMapping("speciality")
    public Speciality saveSpeciality(@RequestBody Speciality speciality) {
        validate(speciality, "speciality");
        SpecialityEntity result = specialityService.save(speciality);
        return specialityConverter.convertToDto(result);
    }

    @DeleteMapping("speciality/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSpeciality(@PathVariable("id") Long id) {
        SpecialityEntity specialityEntity = specialityService.getById(id);
        specialityService.delete(specialityEntity);
    }

    private void validate(Speciality speciality, String dtoName) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(speciality, dtoName);
        specialityValidator.validate(speciality, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }
}
