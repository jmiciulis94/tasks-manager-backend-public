package net.smarttechsolutions.tasksmanager.backend.controller;

import net.smarttechsolutions.tasksmanager.backend.Application;
import net.smarttechsolutions.tasksmanager.backend.converter.PeriodConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.MonthlyReport;
import net.smarttechsolutions.tasksmanager.backend.dto.Period;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import net.smarttechsolutions.tasksmanager.backend.service.PeriodService;
import net.smarttechsolutions.tasksmanager.backend.service.TimesheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Application.PATH)
@CrossOrigin
public class PeriodController {

    @Autowired
    private PeriodService periodService;

    @Autowired
    private TimesheetService timesheetService;

    @Autowired
    private PeriodConverter periodConverter;

    @GetMapping("period/{id}")
    public Period getPeriodById(@PathVariable("id") Long id) {
        PeriodEntity periodEntity = periodService.getById(id);
        return periodConverter.convertToDto(periodEntity);
    }

    @GetMapping("period")
    public List<Period> findAllPeriods() {
        return periodService.getAll();
    }

    @GetMapping("report/period/{id}")
    public MonthlyReport getMonthlyReport(@PathVariable("id") Long id) {
        return timesheetService.getMonthlyReport(id);
    }
}
