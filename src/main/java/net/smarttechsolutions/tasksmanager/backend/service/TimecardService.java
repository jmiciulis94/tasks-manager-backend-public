package net.smarttechsolutions.tasksmanager.backend.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.smarttechsolutions.tasksmanager.backend.constants.PayType;
import net.smarttechsolutions.tasksmanager.backend.converter.TimecardConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Timecard;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodBudgetEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.TimecardEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.TimesheetEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.PeriodRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TimecardRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TimesheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TimecardService {

    @Autowired
    private TimecardRepository timecardRepository;

    @Autowired
    private TimesheetRepository timesheetRepository;

    @Autowired
    private PeriodRepository periodRepository;

    @Autowired
    private TimecardConverter timecardConverter;

    public TimecardEntity getById(Long id) {
        return timecardRepository.getById(id);
    }

    public TimecardEntity save(Timecard timecard) {

        TimecardEntity timecardEntity = timecardConverter.convertToEntity(timecard);
        boolean timesheetExists = false;
        List<TimesheetEntity> timesheetEntityList = timesheetRepository.findAllByDeletedFalse();
        List<TimesheetEntity> filteredTimesheetEntityList = timesheetEntityList.stream()
                .filter(t -> t.getTask().getId().equals(timecardEntity.getTask().getId()))
                .filter(t -> t.getPeriod().getYear().equals(timecardEntity.getDate().getYear()))
                .filter(t -> t.getPeriod().getMonth().equals(timecardEntity.getDate().getMonthValue()))
                .collect(Collectors.toList());
        List<TimesheetEntity> filteredTimesheetEntityListForRates = filteredTimesheetEntityList.stream().filter(t -> t.getTypeOfPay().equals(PayType.RATE)).collect(Collectors.toList());
        List<TimesheetEntity> filteredTimesheetEntityListForHourly = filteredTimesheetEntityList.stream().filter(t -> t.getTypeOfPay().equals(PayType.HOURLY)).collect(Collectors.toList());
        PeriodBudgetEntity periodBudgetEntity = timecardEntity.getTask().getPeriodBudgets().stream()
                .filter(p -> p.getPeriod().getYear().equals(timecardEntity.getDate().getYear()))
                .filter(p -> p.getPeriod().getMonth().equals(timecardEntity.getDate().getMonthValue()))
                .findFirst()
                .orElse(null);

        if (timecardEntity.getEmployee().getTypeOfPay() == PayType.RATE) {
            BigDecimal allEmployeeMinutes = new BigDecimal(timecardEntity.getMinutes());
            BigDecimal moneyUsedForHourly = new BigDecimal(0);
            for (TimesheetEntity otherRateTimesheetEntity : filteredTimesheetEntityListForRates) {
                allEmployeeMinutes = allEmployeeMinutes.add(BigDecimal.valueOf(otherRateTimesheetEntity.getMinutes()));
            }
            for (TimesheetEntity otherHourlyTimesheetEntity : filteredTimesheetEntityListForHourly) {
                moneyUsedForHourly = moneyUsedForHourly.add(otherHourlyTimesheetEntity.getAmount());
            }
            for (TimesheetEntity otherRateTimesheetEntity : filteredTimesheetEntityListForRates) {
                BigDecimal otherRate = otherRateTimesheetEntity.getEmployee().getRate();
                BigDecimal otherMinutes = BigDecimal.valueOf(otherRateTimesheetEntity.getMinutes());
                rateCalculation(otherRateTimesheetEntity, periodBudgetEntity, moneyUsedForHourly, otherMinutes, allEmployeeMinutes, otherRate);
                timesheetRepository.save(otherRateTimesheetEntity);
            }
        }

        for (TimesheetEntity timesheetEntity : filteredTimesheetEntityList) {
            if (timesheetEntity.getEmployee() == timecardEntity.getEmployee()) {
                timesheetEntity.setMinutes(timesheetEntity.getMinutes() + timecardEntity.getMinutes());
                PayType type = timecardEntity.getEmployee().getTypeOfPay();
                if (type == PayType.HOURLY) {
                    BigDecimal payPerHour = timesheetEntity.getPayPerHour();
                    BigDecimal SIXTY = new BigDecimal(60);
                    BigDecimal result = payPerHour.multiply(BigDecimal.valueOf(timecardEntity.getMinutes()).divide(SIXTY, 6, RoundingMode.HALF_UP));
                    if (timesheetEntity.getAmount() != null) {
                        BigDecimal existingPay = timesheetEntity.getAmount();
                        BigDecimal sum = result.add(existingPay);
                        timesheetEntity.setAmount(sum);
                    } else {
                        timesheetEntity.setAmount(result);
                    }
                }
                if (type == PayType.RATE) {
                    BigDecimal allEmployeeMinutes = new BigDecimal(timesheetEntity.getMinutes() + timecardEntity.getMinutes());
                    BigDecimal moneyUsedForHourly = new BigDecimal(0);
                    for (TimesheetEntity filteredTimesheetEntity : filteredTimesheetEntityListForRates) {
                        allEmployeeMinutes = allEmployeeMinutes.add(BigDecimal.valueOf(filteredTimesheetEntity.getMinutes()));
                    }
                    for (TimesheetEntity otherHourlyTimesheetEntity : filteredTimesheetEntityListForHourly) {
                        moneyUsedForHourly = moneyUsedForHourly.add(otherHourlyTimesheetEntity.getAmount());
                    }
                    BigDecimal rate = timecardEntity.getEmployee().getRate();
                    BigDecimal minutes = BigDecimal.valueOf(timecardEntity.getMinutes());
                    rateCalculation(timesheetEntity, periodBudgetEntity, moneyUsedForHourly, minutes, allEmployeeMinutes, rate);
                }
                timesheetRepository.save(timesheetEntity);
                timesheetExists = true;
            }
        }
        if (!timesheetExists) {
            TimesheetEntity timesheetEntity = new TimesheetEntity();
            Boolean ifPeriodExists = periodRepository.existsByYearAndMonthAndDeletedFalse(timecardEntity.getDate().getYear(), timecardEntity.getDate().getMonthValue());
            if (!ifPeriodExists) {
                PeriodEntity periodEntity = new PeriodEntity();
                periodEntity.setYear(timecardEntity.getDate().getYear());
                periodEntity.setMonth(timecardEntity.getDate().getMonthValue());
                periodRepository.save(periodEntity);
                timesheetEntity.setPeriod(periodEntity);
            } else {
                timesheetEntity.setPeriod(periodRepository.getByYearAndMonthAndDeletedFalse(timecardEntity.getDate().getYear(), timecardEntity.getDate().getMonthValue()));
            }
            timesheetEntity.setMinutes(timecardEntity.getMinutes());
            PayType type = timecardEntity.getEmployee().getTypeOfPay();
            switch (type) {
                case NONE:
                    BigDecimal resultNone = new BigDecimal(0);
                    timesheetEntity.setAmount(resultNone);
                    break;
                case HOURLY:
                    BigDecimal payPerHour = timecardEntity.getEmployee().getPayPerHour();
                    BigDecimal SIXTY = new BigDecimal(60);
                    BigDecimal hours = BigDecimal.valueOf(timecardEntity.getMinutes()).divide(SIXTY, 6, RoundingMode.HALF_UP);
                    BigDecimal resultHourly = payPerHour.multiply(hours);
                    timesheetEntity.setAmount(resultHourly);
                    break;
                case RATE:
                    BigDecimal allEmployeeMinutes = new BigDecimal(timecardEntity.getMinutes());
                    BigDecimal moneyUsedForHourly = new BigDecimal(0);
                    for (TimesheetEntity filteredTimesheetEntity : filteredTimesheetEntityListForRates) {
                        allEmployeeMinutes = allEmployeeMinutes.add(BigDecimal.valueOf(filteredTimesheetEntity.getMinutes()));
                    }
                    for (TimesheetEntity otherHourlyTimesheetEntity : filteredTimesheetEntityListForHourly) {
                        moneyUsedForHourly = moneyUsedForHourly.add(otherHourlyTimesheetEntity.getAmount());
                    }
                    BigDecimal rate = timecardEntity.getEmployee().getRate();
                    BigDecimal minutes = BigDecimal.valueOf(timecardEntity.getMinutes());
                    rateCalculation(timesheetEntity, periodBudgetEntity, moneyUsedForHourly, minutes, allEmployeeMinutes, rate);
                    break;
            }
            timesheetEntity.setTypeOfPay(timecardEntity.getEmployee().getTypeOfPay());
            if (timecardEntity.getEmployee().getPayPerHour() != null) {
                timesheetEntity.setPayPerHour(timecardEntity.getEmployee().getPayPerHour());
            }
            if (timecardEntity.getEmployee().getRate() != null) {
                timesheetEntity.setRate(timecardEntity.getEmployee().getRate());
            }
            timesheetEntity.setEmployee(timecardEntity.getEmployee());
            timesheetEntity.setTask(timecardEntity.getTask());
            timesheetRepository.save(timesheetEntity);
        }
        return timecardRepository.save(timecardEntity);
    }

    public List<Timecard> getAllByEmployeeId(Long employeeId) {
        List<Timecard> timecardList = new ArrayList<>();
        List<TimecardEntity> timecardEntityList = timecardRepository.findAllByEmployeeIdAndDeletedFalse(employeeId);
        for (TimecardEntity timecardEntity : timecardEntityList) {
            Timecard task = timecardConverter.convertToDto(timecardEntity);
            timecardList.add(task);
        }

        return timecardList;
    }

    @Transactional
    public void delete(TimecardEntity timecardEntity) {
        timecardEntity.setDeleted(true);
        List<TimesheetEntity> timesheetEntityList = timesheetRepository.findAllByDeletedFalse();
        for (TimesheetEntity timesheetEntity : timesheetEntityList) {
            if (timecardEntity.getDate().getYear() == timesheetEntity.getPeriod().getYear() && timecardEntity.getDate().getMonthValue() == timesheetEntity.getPeriod().getMonth()
                    && timecardEntity.getEmployee() == timesheetEntity.getEmployee() && timecardEntity.getTask() == timesheetEntity.getTask()) {
                if (timecardEntity.getMinutes() >= timesheetEntity.getMinutes()) {
                    timesheetEntity.setDeleted(true);
                    timesheetRepository.save(timesheetEntity);
                } else {
                    timesheetEntity.setMinutes(timesheetEntity.getMinutes() - timecardEntity.getMinutes());
                    PayType type = timesheetEntity.getTypeOfPay();
                    if (type == PayType.HOURLY) {
                        BigDecimal payPerHour = timesheetEntity.getPayPerHour();
                        BigDecimal SIXTY = new BigDecimal(60);
                        BigDecimal hours = BigDecimal.valueOf(timesheetEntity.getMinutes()).divide(SIXTY, 6, RoundingMode.HALF_UP);
                        BigDecimal resultHourly = payPerHour.multiply(hours);
                        timesheetEntity.setAmount(resultHourly);
                    }
                }
            }
        }
        timecardRepository.save(timecardEntity);
    }

    private void rateCalculation(TimesheetEntity timesheetEntity, PeriodBudgetEntity periodBudgetEntity, BigDecimal moneyUsedForHourly, BigDecimal minutes, BigDecimal allEmployeeMinutes, BigDecimal rate) {
        if (periodBudgetEntity != null) {
            BigDecimal periodBudgetAmount = periodBudgetEntity.getAmount();
            BigDecimal periodBudgetRemaining = periodBudgetAmount.subtract(moneyUsedForHourly);
            BigDecimal timeDifference = minutes.divide(allEmployeeMinutes, 2, RoundingMode.HALF_UP);
            BigDecimal result = periodBudgetRemaining.multiply(timeDifference);
            BigDecimal resultRate = result.multiply(rate);
            timesheetEntity.setAmount(resultRate);
        } else {
            timesheetEntity.setAmount(null);
        }
    }
}
