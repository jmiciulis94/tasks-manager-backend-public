package net.smarttechsolutions.tasksmanager.backend.service;

import net.smarttechsolutions.tasksmanager.backend.converter.EmployeeConverter;
import net.smarttechsolutions.tasksmanager.backend.converter.TimesheetConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.*;
import net.smarttechsolutions.tasksmanager.backend.dto.Employee;
import net.smarttechsolutions.tasksmanager.backend.entity.*;
import net.smarttechsolutions.tasksmanager.backend.repository.EmployeeRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.ProjectRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TaskRepository;
import net.smarttechsolutions.tasksmanager.backend.repository.TimesheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimesheetService {

    @Autowired
    private EmployeeConverter employeeConverter;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TimesheetRepository timesheetRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TimesheetConverter timesheetConverter;

    public TimesheetEntity getById(Long id) {
        return timesheetRepository.getById(id);
    }

    public TimesheetEntity save(TimesheetEntity timesheetEntity) {
        return timesheetRepository.save(timesheetEntity);
    }

    public EmployeeReport getEmployeeReport(Long employeeId) {
        List<Timesheet> timesheetList = new ArrayList<>();
        List<TimesheetEntity> timesheetEntityList = timesheetRepository.findAllByEmployeeIdAndDeletedFalse(employeeId);
        for (TimesheetEntity timesheetEntity : timesheetEntityList) {
            Timesheet timesheet = timesheetConverter.convertToDto(timesheetEntity);
            timesheetList.add(timesheet);
        }

        List<ProjectHeader> projectHeaders = new ArrayList<>();
        List<Long> projectIds = timesheetEntityList.stream().map(p -> p.getTask().getProject().getId()).distinct().collect(Collectors.toList());
        List<ProjectEntity> projectEntities = new ArrayList<>(projectRepository.findByIdIn(projectIds));

        List<TaskHeader> taskHeaders = new ArrayList<>();
        List<Long> taskIds = timesheetList.stream().map(t -> t.getTaskId()).distinct().collect(Collectors.toList());
        List<TaskEntity> taskEntities = new ArrayList<>(taskRepository.findByIdIn(taskIds));
        List<String> dates = timesheetList.stream().map(t -> t.getFormattedDate()).distinct().collect(Collectors.toList());
        List<PeriodTimesheet> entries = new ArrayList<>();

        for (ProjectEntity projectEntity : projectEntities) {
            ProjectHeader projectHeader = new ProjectHeader();
            Integer numberOfTasks = Math.toIntExact(taskEntities.stream().filter(t -> t.getProject().getId().equals(projectEntity.getId())).count());
            projectHeader.setProjectId(projectEntity.getId());
            projectHeader.setTitle(projectEntity.getTitle());
            projectHeader.setNumberOfTasks(numberOfTasks);
            projectHeaders.add(projectHeader);
        }

        Integer totalMinutes = 0;
        BigDecimal totalAmount = new BigDecimal(0);

        for (String date : dates) {
            Period period = new Period();
            List<Timesheet> timesheetsForPeriod = new ArrayList<>();
            PeriodTimesheet entry = new PeriodTimesheet();
            Timesheet sumTimesheet = new Timesheet();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (TaskEntity taskEntity : taskEntities) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getTaskId().equals(taskEntity.getId()) && t.getFormattedDate().equals(date)).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                    if (period.getYear() == null) {
                        period = timesheet.getPeriod();
                    }
                }
                timesheetsForPeriod.add(timesheet);
            }
            sumTimesheet.setMinutes(sumMinutes);
            sumTimesheet.setAmount(sumAmount);
            timesheetsForPeriod.add(sumTimesheet);
            entry.setPeriod(period);
            entry.setTimesheets(timesheetsForPeriod);
            entries.add(entry);
        }

        for (TaskEntity taskEntity : taskEntities) {
            TaskHeader taskHeader = new TaskHeader();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (String date : dates) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getTaskId().equals(taskEntity.getId()) && t.getFormattedDate().equals(date)).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                }
            }
            taskHeader.setTaskId(taskEntity.getId());
            taskHeader.setTitle(taskEntity.getTitle());
            taskHeader.setTotalMinutes(sumMinutes);
            taskHeader.setTotalAmount(sumAmount);
            taskHeaders.add(taskHeader);
            totalMinutes += sumMinutes;
            totalAmount = totalAmount.add(sumAmount);
        }

        Totals totals = new Totals();

        totals.setMinutes(totalMinutes);
        totals.setAmount(totalAmount);

        EmployeeReport employeeReport = new EmployeeReport();

        employeeReport.setProjects(projectHeaders);
        employeeReport.setTasks(taskHeaders);
        employeeReport.setEntries(entries);
        employeeReport.setTotals(totals);

        return employeeReport;
    }

    public MonthlyReport getMonthlyReport(Long id) {
        List<Timesheet> timesheetList = new ArrayList<>();
        List<TimesheetEntity> timesheetEntityList = timesheetRepository.findAllByPeriodIdAndDeletedFalse(id);
        for (TimesheetEntity timesheetEntity : timesheetEntityList) {
            Timesheet timesheet = timesheetConverter.convertToDto(timesheetEntity);
            timesheetList.add(timesheet);
        }

        List<TaskHeader> taskHeaders = new ArrayList<>();
        List<Long> taskIds = timesheetList.stream().map(t -> t.getTaskId()).distinct().collect(Collectors.toList());
        List<TaskEntity> taskEntities = new ArrayList<>(taskRepository.findByIdIn(taskIds));
        List<Long> employeeIds = timesheetList.stream().map(t -> t.getEmployeeId()).distinct().collect(Collectors.toList());
        List<EmployeeEntity> employeeEntities = new ArrayList<>(employeeRepository.findByIdIn(employeeIds));
        List<EmployeeTimesheet> entries = new ArrayList<>();

        Integer totalMinutes = 0;
        BigDecimal totalAmount = new BigDecimal(0);

        for (EmployeeEntity employeeEntity : employeeEntities) {
            List<Timesheet> timesheetsForEmployee = new ArrayList<>();
            EmployeeTimesheet entry = new EmployeeTimesheet();
            Timesheet sumTimesheet = new Timesheet();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (TaskEntity taskEntity : taskEntities) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getTaskId().equals(taskEntity.getId()) && t.getEmployeeId().equals(employeeEntity.getId())).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                }
                timesheetsForEmployee.add(timesheet);
            }
            sumTimesheet.setMinutes(sumMinutes);
            sumTimesheet.setAmount(sumAmount);
            timesheetsForEmployee.add(sumTimesheet);
            Employee employeeDto = employeeConverter.convertToDto(employeeEntity);
            entry.setEmployee(employeeDto);
            entry.setTimesheets(timesheetsForEmployee);
            entries.add(entry);
        }

        for (TaskEntity taskEntity : taskEntities) {
            TaskHeader taskHeader = new TaskHeader();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (EmployeeEntity employeeEntity : employeeEntities) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getTaskId().equals(taskEntity.getId()) && t.getEmployeeId().equals(employeeEntity.getId())).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                }
            }
            taskHeader.setTaskId(taskEntity.getId());
            taskHeader.setTitle(taskEntity.getTitle());
            taskHeader.setTotalMinutes(sumMinutes);
            taskHeader.setTotalAmount(sumAmount);
            taskHeaders.add(taskHeader);
            totalMinutes += sumMinutes;
            totalAmount = totalAmount.add(sumAmount);
        }

        Totals totals = new Totals();

        totals.setMinutes(totalMinutes);
        totals.setAmount(totalAmount);

        MonthlyReport monthlyReport = new MonthlyReport();

        monthlyReport.setEntries(entries);
        monthlyReport.setTasks(taskHeaders);
        monthlyReport.setTotals(totals);

        return monthlyReport;
    }

    public TaskReport getTaskReport(Long id) {
        List<Timesheet> timesheetList = new ArrayList<>();
        List<TimesheetEntity> timesheetEntityList = timesheetRepository.findAllByTaskIdAndDeletedFalse(id);
        for (TimesheetEntity timesheetEntity : timesheetEntityList) {
            Timesheet timesheet = timesheetConverter.convertToDto(timesheetEntity);
            timesheetList.add(timesheet);
        }

        List<EmployeeHeader> employeeHeaders = new ArrayList<>();
        List<Long> employeeIds = timesheetList.stream().map(t -> t.getEmployeeId()).distinct().collect(Collectors.toList());
        List<EmployeeEntity> employeeEntities = new ArrayList<>(employeeRepository.findByIdIn(employeeIds));
        List<String> dates = timesheetList.stream().map(t -> t.getFormattedDate()).distinct().collect(Collectors.toList());
        List<PeriodTimesheet> entries = new ArrayList<>();

        Integer totalMinutes = 0;
        BigDecimal totalAmount = new BigDecimal(0);

        for (String date : dates) {
            Period period = new Period();
            List<Timesheet> timesheetsForPeriod = new ArrayList<>();
            PeriodTimesheet entry = new PeriodTimesheet();
            Timesheet sumTimesheet = new Timesheet();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (EmployeeEntity employeeEntity : employeeEntities) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getEmployeeId().equals(employeeEntity.getId()) && t.getFormattedDate().equals(date)).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                    if (period.getYear() == null) {
                        period = timesheet.getPeriod();
                    }
                }
                timesheetsForPeriod.add(timesheet);
            }
            sumTimesheet.setMinutes(sumMinutes);
            sumTimesheet.setAmount(sumAmount);
            timesheetsForPeriod.add(sumTimesheet);
            entry.setPeriod(period);
            entry.setTimesheets(timesheetsForPeriod);
            entries.add(entry);
        }

        for (EmployeeEntity employeeEntity : employeeEntities) {
            EmployeeHeader employeeHeader = new EmployeeHeader();
            Integer sumMinutes = 0;
            BigDecimal sumAmount = new BigDecimal(0);
            for (String date : dates) {
                Timesheet timesheet = timesheetList.stream().filter(t -> t.getEmployeeId().equals(employeeEntity.getId()) && t.getFormattedDate().equals(date)).findFirst().orElse(null);
                if (timesheet != null) {
                    sumMinutes += timesheet.getMinutes();
                    sumAmount = sumAmount.add(timesheet.getAmount());
                }
            }
            employeeHeader.setEmployeeId(employeeEntity.getId());
            employeeHeader.setFullName(employeeEntity.getFirstName() + " " + employeeEntity.getLastName());
            employeeHeader.setTotalMinutes(sumMinutes);
            employeeHeader.setTotalAmount(sumAmount);
            employeeHeaders.add(employeeHeader);
            totalMinutes += sumMinutes;
            totalAmount = totalAmount.add(sumAmount);
        }

        Totals totals = new Totals();

        totals.setMinutes(totalMinutes);
        totals.setAmount(totalAmount);

        TaskReport taskReport = new TaskReport();

        taskReport.setEmployees(employeeHeaders);
        taskReport.setEntries(entries);
        taskReport.setTotals(totals);

        return taskReport;
    }

    @Transactional
    public void delete(TimesheetEntity timesheetEntity) {
        timesheetEntity.setDeleted(true);
        timesheetRepository.save(timesheetEntity);
    }
}
