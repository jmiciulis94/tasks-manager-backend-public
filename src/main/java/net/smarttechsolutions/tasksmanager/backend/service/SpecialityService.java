package net.smarttechsolutions.tasksmanager.backend.service;

import java.util.ArrayList;
import java.util.List;
import net.smarttechsolutions.tasksmanager.backend.converter.SpecialityConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Speciality;
import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SpecialityService {

    @Autowired
    private SpecialityRepository specialityRepository;

    @Autowired
    private SpecialityConverter specialityConverter;

    public SpecialityEntity getById(Long id) {
        return specialityRepository.getById(id);
    }

    public SpecialityEntity save(Speciality speciality) {
        SpecialityEntity specialityEntity = specialityConverter.convertToEntity(speciality);
        return specialityRepository.save(specialityEntity);
    }

    public List<Speciality> getAll() {
        List<Speciality> specialityList = new ArrayList<>();
        List<SpecialityEntity> specialityEntityList = specialityRepository.findAllByDeletedFalse();
        for (SpecialityEntity specialityEntity : specialityEntityList) {
            Speciality speciality = specialityConverter.convertToDto(specialityEntity);
            specialityList.add(speciality);
        }
        return specialityList;
    }

    @Transactional
    public void delete(SpecialityEntity specialityEntity) {
        specialityEntity.setDeleted(true);
        specialityRepository.save(specialityEntity);
    }
}
