package net.smarttechsolutions.tasksmanager.backend.service;

import java.util.ArrayList;
import java.util.List;
import net.smarttechsolutions.tasksmanager.backend.converter.PeriodConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Period;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.PeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeriodService {

    @Autowired
    private PeriodRepository periodRepository;

    @Autowired
    private PeriodConverter periodConverter;

    public PeriodEntity getById(Long id) {
        return periodRepository.getById(id);
    }

    public List<Period> getAll() {
        List<Period> periodList = new ArrayList<>();
        List<PeriodEntity> periodEntityList = periodRepository.findAllByDeletedFalse();
        for (PeriodEntity periodEntity : periodEntityList) {
            Period period = periodConverter.convertToDto(periodEntity);
            periodList.add(period);
        }
        return periodList;
    }
}
