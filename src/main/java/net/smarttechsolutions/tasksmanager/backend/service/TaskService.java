package net.smarttechsolutions.tasksmanager.backend.service;

import java.util.ArrayList;
import java.util.List;
import net.smarttechsolutions.tasksmanager.backend.converter.TaskConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskService {

    @Autowired
    private TaskConverter taskConverter;

    @Autowired
    private TaskRepository taskRepository;

    public TaskEntity getById(Long id) {
        return taskRepository.getById(id);
    }

    public TaskEntity save(Task task) {
        TaskEntity taskEntity = taskConverter.convertToEntity(task);
        return taskRepository.save(taskEntity);
    }

    public List<Task> getAll() {
        List<Task> taskList = new ArrayList<>();
        List<TaskEntity> taskEntityList = taskRepository.findAllByDeletedFalse();
        for (TaskEntity taskEntity : taskEntityList) {
            Task task = taskConverter.convertToDto(taskEntity);
            taskList.add(task);
        }
        return taskList;
    }

    @Transactional
    public void delete(TaskEntity taskEntity) {
        taskEntity.setDeleted(true);
        taskRepository.save(taskEntity);
    }
}
