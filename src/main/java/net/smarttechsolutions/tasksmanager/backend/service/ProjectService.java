package net.smarttechsolutions.tasksmanager.backend.service;

import java.util.ArrayList;
import java.util.List;
import net.smarttechsolutions.tasksmanager.backend.converter.ProjectConverter;
import net.smarttechsolutions.tasksmanager.backend.converter.TaskConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Project;
import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.ProjectEntity;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService {

    @Autowired
    private ProjectConverter projectConverter;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskConverter taskConverter;

    public ProjectEntity getById(Long id) {
        return projectRepository.getById(id);
    }

    public ProjectEntity save(Project project) {
        ProjectEntity projectEntity = projectConverter.convertToEntity(project);
        return projectRepository.save(projectEntity);
    }

    public List<Project> getAll() {
        List<Project> projectList = new ArrayList<>();
        List<ProjectEntity> projectEntityList = projectRepository.findAllByDeletedFalse();
        for (ProjectEntity projectEntity : projectEntityList) {
            Project project = projectConverter.convertToDto(projectEntity);
            projectList.add(project);
        }
        return projectList;
    }

    public List<Task> getTasksByProjectId(Long id) {
        List<Task> taskList = new ArrayList<>();
        List<TaskEntity> taskEntityList = projectRepository.findById(id).getTasks();
        for (TaskEntity taskEntity : taskEntityList) {
            if (!taskEntity.getDeleted()) {
                Task task = taskConverter.convertToDto(taskEntity);
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Transactional
    public void delete(ProjectEntity projectEntity) {
        projectEntity.setDeleted(true);
        projectRepository.save(projectEntity);
    }
}
