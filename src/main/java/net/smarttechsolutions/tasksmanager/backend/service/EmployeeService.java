package net.smarttechsolutions.tasksmanager.backend.service;

import net.smarttechsolutions.tasksmanager.backend.converter.EmployeeConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.Employee;
import net.smarttechsolutions.tasksmanager.backend.entity.EmployeeEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeConverter employeeConverter;

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeEntity getById(Long id) {
        return employeeRepository.getById(id);
    }

    public EmployeeEntity save(Employee employee) {
        EmployeeEntity employeeEntity = employeeConverter.convertToEntity(employee);
        return employeeRepository.save(employeeEntity);
    }

    public List<Employee> getAll() {
        List<Employee> employeeList = new ArrayList<>();
        List<EmployeeEntity> employeeEntityList = employeeRepository.findAllByDeletedFalse();
        for (EmployeeEntity employeeEntity : employeeEntityList) {
            Employee employee = employeeConverter.convertToDto(employeeEntity);
            employeeList.add(employee);
        }
        return employeeList;
    }

    @Transactional
    public void delete(EmployeeEntity employeeEntity) {
        employeeEntity.setDeleted(true);
        employeeRepository.save(employeeEntity);
    }
}
