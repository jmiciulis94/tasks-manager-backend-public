package net.smarttechsolutions.tasksmanager.backend.service;

import java.util.ArrayList;
import java.util.List;
import net.smarttechsolutions.tasksmanager.backend.converter.UserConverter;
import net.smarttechsolutions.tasksmanager.backend.dto.User;
import net.smarttechsolutions.tasksmanager.backend.entity.UserEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserConverter userConverter;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    public UserEntity getById(Long id) {
        return userRepository.getById(id);
    }

//    @Transactional
//    public User save(User user) {
//        if (user.getPassword() != null) {
//            user.setPassword(passwordEncoder.encode(user.getPassword()));
//        } else {
//            User existingUser = userRepository.getById(user.getId());
//            user.setPassword(existingUser.getPassword());
//        }
//
//        return userRepository.save(user);
//    }

    public UserEntity save(User user) {
        UserEntity userEntity = userConverter.convertToEntity(user);
        return userRepository.save(userEntity);
    }

    public List<User> getAll() {
        List<User> userList = new ArrayList<>();
        List<UserEntity> userEntityList = userRepository.findAllByDeletedFalse();
        for (UserEntity userEntity : userEntityList) {
            User user = userConverter.convertToDto(userEntity);
            userList.add(user);
        }

        return userList;
    }

    @Transactional
    public void delete(UserEntity userEntity) {
        userEntity.setDeleted(true);
        userRepository.save(userEntity);
    }
}
