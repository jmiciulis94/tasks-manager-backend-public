package net.smarttechsolutions.tasksmanager.backend.constants;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PeriodType {

    @JsonProperty("Monthly")
    MONTHLY,
    @JsonProperty("Weekly")
    WEEKLY;
}
