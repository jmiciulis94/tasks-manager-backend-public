package net.smarttechsolutions.tasksmanager.backend.constants;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FirstDayType {

    @JsonProperty("Monday")
    MONDAY,
    @JsonProperty("Sunday")
    SUNDAY;
}
