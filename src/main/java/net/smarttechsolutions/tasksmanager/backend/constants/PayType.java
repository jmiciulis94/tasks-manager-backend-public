package net.smarttechsolutions.tasksmanager.backend.constants;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PayType {

    @JsonProperty("RATE")
    RATE,
    @JsonProperty("HOURLY")
    HOURLY,
    @JsonProperty("NONE")
    NONE;
}
