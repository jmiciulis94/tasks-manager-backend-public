package net.smarttechsolutions.tasksmanager.backend.utils;

public class TimeUtils {
    public String minutesToTime(Integer minutes) {
        int hours = minutes / 60;
        int remainingMinutes = minutes % 60;
        String hoursString = Integer.toString(hours);
        String minutesString = Integer.toString(remainingMinutes);
        return hoursString + "h " + minutesString + "m";
    }
}
