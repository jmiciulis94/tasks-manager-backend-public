package net.smarttechsolutions.tasksmanager.backend.utils;

import net.smarttechsolutions.tasksmanager.backend.constants.PayType;

import java.math.BigDecimal;

public class PayUtils {
    public String getPayRate(PayType typeOfPay, BigDecimal rate, BigDecimal payPerHour) {
        switch (typeOfPay) {
            case NONE:
                return "0€/h";
            case RATE:
                return rate.toString();
            case HOURLY:
                return payPerHour.toString() + "€/h";
            default:
                return "Unknown";
        }
    }
}
