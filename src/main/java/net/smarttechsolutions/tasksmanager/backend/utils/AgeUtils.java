package net.smarttechsolutions.tasksmanager.backend.utils;

import java.time.LocalDate;
import java.time.Period;

public class AgeUtils {
    public Integer ageFromDateOfBirth(LocalDate dateOfBirth) {
        LocalDate today = LocalDate.now();
        Period period = Period.between(dateOfBirth, today);

        return period.getYears();
    }
}
