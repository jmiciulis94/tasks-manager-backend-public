package net.smarttechsolutions.tasksmanager.backend.config;

import net.smarttechsolutions.tasksmanager.backend.exception.ServerError;
import net.smarttechsolutions.tasksmanager.backend.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlers {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ServerError handleValidationException(HttpServletResponse response, ValidationException ex) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        String header = messageSource.getMessage(ex.getErrors().getGlobalError(), Locale.getDefault());
        List<String> items = ex.getErrors().getFieldErrors().stream().map(e -> messageSource.getMessage(e, Locale.getDefault())).collect(Collectors.toList());

        return new ServerError(header, items);
    }
}
