package net.smarttechsolutions.tasksmanager.backend.repository;

import java.time.LocalDate;
import net.smarttechsolutions.tasksmanager.backend.entity.TimecardEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface TimecardRepository extends Repository<TimecardEntity, Long> {

    TimecardEntity save(TimecardEntity timecardEntity);

    TimecardEntity getById(Long id);

    List<TimecardEntity> findAllByEmployeeIdAndDeletedFalse(Long employeeId);

    List<TimecardEntity> findAllByDateAndDeletedFalse(LocalDate date);

}
