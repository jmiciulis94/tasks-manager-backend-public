package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.TimesheetEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface TimesheetRepository extends Repository<TimesheetEntity, Long> {

    TimesheetEntity save(TimesheetEntity timesheetEntity);

    TimesheetEntity getById(Long id);

    List<TimesheetEntity> findAllByDeletedFalse();

    List<TimesheetEntity> findAllByEmployeeIdAndDeletedFalse(Long id);

    List<TimesheetEntity> findAllByTaskIdAndDeletedFalse(Long id);

    List<TimesheetEntity> findAllByPeriodIdAndDeletedFalse(Long id);

    List<TimesheetEntity> findByIdIn(List<Long> ids);
}
