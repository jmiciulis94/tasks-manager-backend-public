package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.ProjectEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ProjectRepository extends Repository<ProjectEntity, Long> {

    ProjectEntity save(ProjectEntity projectEntity);

    ProjectEntity getById(Long id);

    ProjectEntity findById(Long id);

    List<ProjectEntity> findAllByDeletedFalse();

    List<ProjectEntity> findByIdIn(List<Long> ids);

}
