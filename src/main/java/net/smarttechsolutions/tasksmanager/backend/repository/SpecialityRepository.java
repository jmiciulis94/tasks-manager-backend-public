package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.SpecialityEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface SpecialityRepository extends Repository<SpecialityEntity, Long> {

    SpecialityEntity save(SpecialityEntity specialityEntity);

    SpecialityEntity getById(Long id);

    List<SpecialityEntity> findAllByDeletedFalse();

    List<SpecialityEntity> findByIdIn(List<Long> ids);
}
