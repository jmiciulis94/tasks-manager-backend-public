package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface PeriodRepository extends Repository<PeriodEntity, Long> {

    PeriodEntity save(PeriodEntity periodEntity);

    PeriodEntity getById(Long id);

    List<PeriodEntity> findAllByDeletedFalse();

    Boolean existsByYearAndMonthAndDeletedFalse(Integer year, Integer month);

    PeriodEntity getByYearAndMonthAndDeletedFalse(Integer year, Integer month);

}
