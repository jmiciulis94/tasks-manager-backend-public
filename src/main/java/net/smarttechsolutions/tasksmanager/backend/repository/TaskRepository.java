package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface TaskRepository extends Repository<TaskEntity, Long> {

    TaskEntity save(TaskEntity taskEntity);

    TaskEntity getById(Long id);

    TaskEntity findById(Long id);

    List<TaskEntity> findAllByDeletedFalse();

    List<TaskEntity> findByIdIn(List<Long> ids);

}
