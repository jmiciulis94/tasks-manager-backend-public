package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.UserEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserRepository extends Repository<UserEntity, Long> {

    UserEntity save(UserEntity userEntity);

    UserEntity getById(Long id);

    List<UserEntity> findAllByDeletedFalse();

    UserEntity findByEmail(String email);

    UserEntity findByUsername(String username);

}
