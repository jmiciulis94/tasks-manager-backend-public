package net.smarttechsolutions.tasksmanager.backend.repository;

import net.smarttechsolutions.tasksmanager.backend.entity.EmployeeEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface EmployeeRepository extends Repository<EmployeeEntity, Long> {

    EmployeeEntity save(EmployeeEntity employeeEntity);

    EmployeeEntity getById(Long id);

    EmployeeEntity findById(Long id);

    EmployeeEntity findByEmailAndDeletedFalse(String email);

    List<EmployeeEntity> findAllByDeletedFalse();

    List<EmployeeEntity> findByIdIn(List<Long> ids);

}
