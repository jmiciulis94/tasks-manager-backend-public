package net.smarttechsolutions.tasksmanager.backend.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.PayType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class EmployeeEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false, length = 100)
    private String firstName;

    @Column(name = "last_name", length = 100)
    private String lastName;

    @Column(name = "email", length = 256)
    private String email;

    @Column(name = "phone", length = 15)
    private String phone;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "type_of_pay", length = 20)
    @Enumerated(value = EnumType.STRING)
    private PayType typeOfPay = PayType.NONE;

    @Column(name = "payPerHour")
    private BigDecimal payPerHour;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @JoinTable(name = "employee_speciality", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "speciality_id"))
    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @Valid
    private List<SpecialityEntity> specialities = new ArrayList<>();

    @Column(name = "user_id")
    private Long userId;
}
