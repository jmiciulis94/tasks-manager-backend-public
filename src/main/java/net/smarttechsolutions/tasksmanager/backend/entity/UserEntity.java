package net.smarttechsolutions.tasksmanager.backend.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.FirstDayType;
import net.smarttechsolutions.tasksmanager.backend.constants.PeriodType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity
@Table(name = "user", schema = "tasks_manager")
public class UserEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", length = 50)
    private String firstName;

    @Column(name = "last_name", length = 50)
    private String lastName;

    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 256)
    private String password;

    @Column(name = "email", nullable = false, length = 256)
    private String email;

    @Column(name = "currency", nullable = false, length = 3)
    private String currency;

    @Column(name = "date_format", nullable = false, length = 20)
    private String dateFormat;

    @Column(name = "number_format", nullable = false, length = 20)
    private String numberFormat;

    @Column(name = "period_type", length = 10)
    @Enumerated(value = EnumType.STRING)
    private PeriodType periodType = PeriodType.MONTHLY;

    @Column(name = "first_day_of_the_week", length = 10)
    @Enumerated(value = EnumType.STRING)
    private FirstDayType firstDayOfTheWeek;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SpecialityEntity> specialities = new ArrayList<>();
}
