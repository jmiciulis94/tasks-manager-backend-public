package net.smarttechsolutions.tasksmanager.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "period")
public class PeriodEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "year", nullable = false, length = 4)
    private Integer year;

    @Column(name = "month", length = 2)
    private Integer month;

    @Column(name = "week", length = 2)
    private Integer week;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @Column(name = "user_id") // toDo: period'ai global visiems useriams?
    private Long userId;
}
