package net.smarttechsolutions.tasksmanager.backend.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.PayType;

@Getter
@Setter
@Entity
@Table(name = "timesheet")
public class TimesheetEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "minutes", nullable = false)
    private Integer minutes;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "type_of_pay", length = 50)
    @Enumerated(value = EnumType.STRING)
    private PayType typeOfPay = PayType.NONE;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "payPerHour")
    private BigDecimal payPerHour;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    @JoinColumn(name = "period_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PeriodEntity period;

    @JoinColumn(name = "task_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TaskEntity task;

    @JoinColumn(name = "employee_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private EmployeeEntity employee;
}
