package net.smarttechsolutions.tasksmanager.backend.validator;

import net.smarttechsolutions.tasksmanager.backend.dto.Project;
import net.smarttechsolutions.tasksmanager.backend.entity.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ProjectValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return ProjectEntity.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Project project = (Project) target;

        if (project.getEstimatedEndDate() != null) {
            if (project.getEstimatedEndDate().compareTo(project.getStartDate()) <= 0) {
                errors.rejectValue("estimatedEndDate", "Custom");
            }
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }
}
