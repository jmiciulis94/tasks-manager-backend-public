package net.smarttechsolutions.tasksmanager.backend.validator;

import net.smarttechsolutions.tasksmanager.backend.dto.Timesheet;
import net.smarttechsolutions.tasksmanager.backend.entity.TimesheetEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TimesheetValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Timesheet.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
