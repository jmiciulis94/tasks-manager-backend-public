package net.smarttechsolutions.tasksmanager.backend.validator;

import net.smarttechsolutions.tasksmanager.backend.constants.PayType;
import net.smarttechsolutions.tasksmanager.backend.dto.Employee;
import net.smarttechsolutions.tasksmanager.backend.entity.EmployeeEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.EmployeeRepository;
import net.smarttechsolutions.tasksmanager.backend.utils.AgeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Component
public class EmployeeValidator implements Validator {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Employee.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        validator.validate(target, errors);
        AgeUtils ageUtils = new AgeUtils();
        Employee employee = (Employee) target;
        PayType type = employee.getTypeOfPay();
        EmployeeEntity existingEmployee = employeeRepository.findByEmailAndDeletedFalse(employee.getEmail());

        if (existingEmployee != null && !existingEmployee.getId().equals(employee.getId())) {
            errors.rejectValue("email", "Unique");
        }

        if (employee.getDateOfBirth() != null) {
            if (employee.getDateOfBirth().compareTo(LocalDate.now()) > 0) {
                errors.rejectValue("dateOfBirth", "Custom");
            }
            if (ageUtils.ageFromDateOfBirth(employee.getDateOfBirth()) > 150) {
                errors.rejectValue("dateOfBirth", "Custom2");
            }
        }

        switch (type) {
            case RATE:
                if (employee.getRate() == null) {
                    errors.rejectValue("rate", "Custom");
                }
                if (employee.getPayPerHour() != null) {
                    errors.rejectValue("rate", "Custom2");
                }
                break;
            case HOURLY:
                if (employee.getPayPerHour() == null) {
                    errors.rejectValue("payPerHour", "Custom");
                }
                if (employee.getRate() != null) {
                    errors.rejectValue("payPerHour", "Custom2");
                }
                break;
            case NONE:
                if (employee.getRate() != null) {
                    errors.rejectValue("rate", "Custom3");
                }
                if (employee.getPayPerHour() != null) {
                    errors.rejectValue("payPerHour", "Custom3");
                }
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }
}
