package net.smarttechsolutions.tasksmanager.backend.validator;

import net.smarttechsolutions.tasksmanager.backend.dto.Task;
import net.smarttechsolutions.tasksmanager.backend.entity.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TaskValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Task.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Task task = (Task) target;

        if (task.getEstimatedEndDate() != null) {
            if (task.getEstimatedEndDate().compareTo(task.getStartDate()) <= 0) {
                errors.rejectValue("estimatedEndDate", "Custom");
            }
        }

        if (task.getIsPaidByFact() && task.getEstimatedCost() == null) {
            errors.rejectValue("estimatedCost", "Custom");
        }

        if (!task.getIsPaidByFact() && task.getEstimatedCost() != null) {
            errors.rejectValue("estimatedCost", "Custom2");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }
}
