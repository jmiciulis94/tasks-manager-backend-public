package net.smarttechsolutions.tasksmanager.backend.validator;

import net.smarttechsolutions.tasksmanager.backend.dto.User;
import net.smarttechsolutions.tasksmanager.backend.entity.UserEntity;
import net.smarttechsolutions.tasksmanager.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        User user = (User) target;

        UserEntity existingUserEmail = userRepository.findByEmail(user.getEmail());

        UserEntity existingUsername = userRepository.findByUsername(user.getUsername());

        if (existingUserEmail != null && !existingUserEmail.getId().equals(user.getId())) {
            errors.rejectValue("email", "Unique");
        }

        if (existingUsername != null && !existingUsername.getId().equals(user.getId())) {
            errors.rejectValue("username", "Unique");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
