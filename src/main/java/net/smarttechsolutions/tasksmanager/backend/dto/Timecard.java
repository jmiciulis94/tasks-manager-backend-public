package net.smarttechsolutions.tasksmanager.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Timecard {

    private Long id;
    @Size(max = 500)
    private String description;
    @Min(1)
    @NotNull
    private Integer minutes;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate date;
    private Long taskId;
    private String taskTitle;
    private Long employeeId;
    private String employeeFullName;
    private Long projectId;
    private String projectTitle;
}
