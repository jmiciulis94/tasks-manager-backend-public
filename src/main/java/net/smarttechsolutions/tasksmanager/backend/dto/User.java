package net.smarttechsolutions.tasksmanager.backend.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.FirstDayType;
import net.smarttechsolutions.tasksmanager.backend.constants.PeriodType;

import java.util.List;

@Getter
@Setter
public class User {

    private Long id;
    @Size(max = 50)
    private String firstName;
    @Size(max = 50)
    private String lastName;
    @NotBlank
    @Size(max = 50)
    private String username;
    @NotBlank
    @Size(max = 256)
    private String password;
    @Size(max = 256)
    @Pattern(regexp = "^$|^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;
    @Size(max = 3)
    @NotNull
    private String currency;
    @Size(max = 20)
    @NotNull
    private String dateFormat;
    @Size(max = 20)
    @NotNull
    private String numberFormat;
    @NotNull
    private PeriodType periodType;
    @NotNull
    private FirstDayType firstDayOfTheWeek;
    private List<Speciality> specialities;
}
