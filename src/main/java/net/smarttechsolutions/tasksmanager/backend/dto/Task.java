package net.smarttechsolutions.tasksmanager.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Task {

    private Long id;
    @Size(max = 100)
    @NotBlank
    private String title;
    @NotNull
    private Boolean isPaidByFact;
    @Min(0)
    @Digits(integer = 12, fraction = 2)
    private BigDecimal estimatedCost;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate startDate;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate estimatedEndDate;
    private Long projectId;
    private List<PeriodBudget> periodBudgets = new ArrayList<>();
}
