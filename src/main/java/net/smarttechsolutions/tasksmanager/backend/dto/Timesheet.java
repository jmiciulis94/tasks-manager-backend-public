package net.smarttechsolutions.tasksmanager.backend.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.PayType;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;

import java.math.BigDecimal;

@Getter
@Setter
public class Timesheet {

    private Long id;
    @Min(1)
    @NotNull
    private Integer minutes;
    @Min(0)
    @NotNull
    private BigDecimal amount;
    @NotNull
    private PayType typeOfPay;
    @Min(0)
    private BigDecimal rate;
    @Min(0)
    private BigDecimal payPerHour;
    private Period period;
    private String payRate;
    private Long taskId;
    private Long employeeId;

    public String getFormattedDate() {
        if (period != null) {
            if (period.getYear() != null && period.getMonth() != null) {
                String newMonth;
                if (period.getMonth().toString().length() == 1) {
                    newMonth = "0" + period.getMonth();
                } else {
                    newMonth = period.getMonth().toString();
                }
                return period.getYear() + "-" + newMonth;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
}
