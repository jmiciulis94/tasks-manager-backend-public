package net.smarttechsolutions.tasksmanager.backend.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PeriodBudget {

    private Long id;
    @Min(0)
    @NotNull
    private BigDecimal amount;
    private Integer month;
    private Integer week;
    @NotNull
    private Integer year;
}
