package net.smarttechsolutions.tasksmanager.backend.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Speciality {

    private Long id;
    @NotBlank
    @Size(max = 50)
    private String title;
}
