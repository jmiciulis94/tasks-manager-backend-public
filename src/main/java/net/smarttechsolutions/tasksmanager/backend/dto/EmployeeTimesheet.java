package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeTimesheet {

    private Employee employee;
    private List<Timesheet> timesheets;
}
