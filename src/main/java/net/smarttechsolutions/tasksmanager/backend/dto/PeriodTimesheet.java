package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.entity.PeriodEntity;

import java.util.List;

@Getter
@Setter
public class PeriodTimesheet {

    private Period period;
    private List<Timesheet> timesheets;
}
