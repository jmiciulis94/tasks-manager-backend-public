package net.smarttechsolutions.tasksmanager.backend.dto;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Period {

    private Long id;
    @NotNull
    private Integer year;
    private Integer month;
    private Integer week;
}