package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TaskReport {

    private List<EmployeeHeader> employees;
    private List<PeriodTimesheet> entries;
    private Totals totals;
}
