package net.smarttechsolutions.tasksmanager.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import net.smarttechsolutions.tasksmanager.backend.constants.PayType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class Employee {

    private Long id;
    @NotBlank
    @Size(max = 100)
    private String firstName;
    @Size(max = 100)
    private String lastName;
    @Size(max = 256)
    @Pattern(regexp = "^$|^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;
    @Size(max = 15)
    @Pattern(regexp = "^$|^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?" +
            "([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)$")
    private String phone;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate dateOfBirth;
    @Min(1)
    @Max(120)
    private Integer age;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate startDate;
    @NotNull
    private PayType typeOfPay;
    @Min(0)
    private BigDecimal payPerHour;
    @Min(0)
    private BigDecimal rate;
    private String payRate;
    private List<Long> specialityIds;
    @Size(max = 50)
    private List<String> specialityTitles;
}
