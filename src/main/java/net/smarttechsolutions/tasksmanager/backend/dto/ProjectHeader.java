package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectHeader {

    private Long projectId;
    private String title;
    private Integer numberOfTasks;
}
