package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MonthlyReport {

    private List<EmployeeTimesheet> entries;
    private List<TaskHeader> tasks;
    private Totals totals;
}
