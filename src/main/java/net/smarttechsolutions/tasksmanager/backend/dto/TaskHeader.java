package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TaskHeader {

    private Long taskId;
    private String title;
    private Integer totalMinutes;
    private BigDecimal totalAmount;
}
