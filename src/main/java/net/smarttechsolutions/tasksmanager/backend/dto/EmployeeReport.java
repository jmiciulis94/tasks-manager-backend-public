package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeReport {

    private List<ProjectHeader> projects;
    private List<TaskHeader> tasks;
    private List<PeriodTimesheet> entries;
    private Totals totals;
}
