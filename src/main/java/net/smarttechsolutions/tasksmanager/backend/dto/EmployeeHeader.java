package net.smarttechsolutions.tasksmanager.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class EmployeeHeader {

    private Long employeeId;
    private String fullName;
    private Integer totalMinutes;
    private BigDecimal totalAmount;
}
